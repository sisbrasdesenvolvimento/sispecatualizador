package br.com.sisbras.sispecAtualizador.ftp.model;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import javax.swing.ImageIcon;

/**
 *
 * @author SAMUEL
 */
public class DisplayAtualizacao extends TrayIcon {

    private static DisplayAtualizacao uniqueInstance;

    private DisplayAtualizacao(Image image) {
        super(image);
        init();
    }

    private void init() {
        this.setImageAutoSize(true);
        final SystemTray tray = SystemTray.getSystemTray();
        try {
            tray.add(this);
        } catch (AWTException e) {
            System.out.println("Componente não pôde ser adicionado a System tray.");
        }
    }

    public static DisplayAtualizacao getInstance() {

        if (uniqueInstance == null) {
            uniqueInstance = new DisplayAtualizacao((new ImageIcon("")).getImage());
        }

        return uniqueInstance;
    }

    public void displayMessage(String caption, String msg) {
        this.displayMessage(caption, msg, TrayIcon.MessageType.INFO);
    }
}
