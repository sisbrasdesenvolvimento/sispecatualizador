package br.com.sisbras.sispecAtualizador.util;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author SAMUEL
 */
public class Util {

    public static String calculaMD5(String arquivo) throws NoSuchAlgorithmException, FileNotFoundException, IOException {

        MessageDigest md = MessageDigest.getInstance("MD5");
        DigestInputStream digin = new DigestInputStream(
                new DataInputStream(
                        new BufferedInputStream(
                                new FileInputStream(new File(arquivo)))), md);

        byte[] buffer = new byte[128];

        while (digin.read(buffer, 0, 128) != - 1) {
            //Do nothing, the digest is calculated automatically as file is read in.      
        }

        digin.close();
        byte[] ret = md.digest();

        StringBuffer result = new StringBuffer();
        for (byte b : ret) {
            result.append(String.format("%02X", b));
        }

        return String.valueOf(result);
    }
}
