package br.com.sisbras.sispecAtualizador.ftp.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SAMUEL
 */
@XmlRootElement(name = "updateDescriptor")
public final class DescriptorUpdate {

    private String versao;
    private String md5;
    private String pathMigrations;

    protected DescriptorUpdate() {
    }
    
    public String getVersao() {
        return versao;
    }

    public void setVersao(String versao) {
        this.versao = versao;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getPathMigrations() {
        return pathMigrations;
    }

    public void setPathMigrations(String pathMigrations) {
        this.pathMigrations = pathMigrations;
    }
}
