package br.com.sisbras.sispecAtualizador.util;

import br.com.sisbras.sispecAtualizador.controle.AtualizadorSispec;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Samuel Paullo
 */
class ConnectionFactory {

    private static final String IP;
    private static final String PORTA;
    private static final String BANCO;
    private static final String USUARIO;
    private static final String SENHA;

    static {
        IP = AtualizadorSispec.PROPERTIES_LOCAL.getProperty("IP");
        PORTA = AtualizadorSispec.PROPERTIES_LOCAL.getProperty("PORTA");
        BANCO = AtualizadorSispec.PROPERTIES_LOCAL.getProperty("DATABASE");
        USUARIO = AtualizadorSispec.PROPERTIES_LOCAL.getProperty("USER", "sa");
        SENHA = AtualizadorSispec.PROPERTIES_LOCAL.getProperty("SENHA", "123456");
    }

    public static Connection getConnection() throws IOException {
        try {

            return DriverManager.getConnection(
                    "jdbc:postgresql://" + IP + ":" + PORTA + "/" + BANCO, "sa", "123456");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
