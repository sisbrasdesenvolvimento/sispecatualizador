package br.com.sisbras.sispecAtualizador.main;

import br.com.sisbras.sispecAtualizador.ftp.model.DescriptorFactory;
import br.com.sisbras.sispecAtualizador.ftp.model.DescriptorUpdate;
import br.com.sisbras.sispecAtualizador.util.Util;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SAMUEL
 */
public class CreateXml {

    public void create(String arg[]) {

        if (arg.length > 0
                && arg[0].equals("create")) {

            try {
                DescriptorUpdate desc = DescriptorFactory.getDescriptorAtualizacaoInstance(null);

                Scanner s = new Scanner(System.in);
                while (true) {

                    System.out.print("Digite a versao do sistema:");
                    desc.setVersao(s.next());

                    if (desc.getVersao() != null
                            && !desc.getVersao().isEmpty()) {
                        break;
                    }
                }

                System.out.println("calculando MD5");

                desc.setMd5(Util.calculaMD5("./sisbras.jar"));

                while (true) {

                    System.out.print("Digite o caminho dos arquivos de migracao:");
                    desc.setPathMigrations(s.next());

                    if (desc.getPathMigrations()!= null
                            && !desc.getPathMigrations().isEmpty()) {
                        break;
                    }
                }

                DescriptorFactory.create(desc);

            } catch (Exception ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }

            System.exit(0);
        }

    }
}
