package br.com.sisbras.sispecAtualizador.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 *
 * @author SAMUEL
 */
public class DAO {

    public void executaSqlScript(InputStream is, String mig) throws IOException, SQLException {

        // Delimiter
        String delimiter = ";";

        // Create scanner
        Scanner scanner;

        try (Connection conn = ConnectionFactory.getConnection()) {

            scanner = new Scanner(is).useDelimiter(delimiter);

            // Loop through the SQL file statements
            Statement currentStatement = null;
            while (scanner.hasNext()) {

                if(hasMigracao(mig, conn)){
                    scanner.next();
                    continue;
                }
                
                // Get statement
                String rawStatement = scanner.next() + delimiter;
                try {
                    // Execute statement
                    currentStatement = conn.createStatement();
                    currentStatement.execute(rawStatement);

                } catch (SQLException e) {
//                    e.printStackTrace();
                    System.out.println(e.getMessage());
                } finally {

                    // Release resources
                    if (currentStatement != null) {
                        try {
                            currentStatement.close();
                        } catch (SQLException e) {
//                        e.printStackTrace();
                            System.out.println(e.getMessage());
                        }
                    }
                    currentStatement = null;
                }
            }

            scanner.close();
        }
    }

    private boolean hasMigracao(String mig, Connection conn) throws SQLException {

        String sql = "select version from schema_version where version = '" + mig + "'";

        ResultSet rs = conn.createStatement().executeQuery(sql);
        
        boolean has = rs.next();

        return has;
    }
}
