package br.com.sisbras.sispecAtualizador.ftp;

import br.com.sisbras.sispecAtualizador.controle.AtualizadorSispec;
import br.com.sisbras.sispecAtualizador.ftp.model.DescriptorFactory;
import br.com.sisbras.sispecAtualizador.ftp.model.DescriptorUpdate;
import br.com.sisbras.sispecAtualizador.ftp.model.DisplayAtualizacao;
import br.com.sisbras.sispecAtualizador.util.DAO;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author SAMUEL
 */
public class ConexaoFTP implements Runnable {

    private FTPClient ftp;
    private DescriptorUpdate desc;

    private Thread t1;

    public ConexaoFTP() throws IOException {
        ftp = new FTPClient();
    }

    public void abrirConexao() throws IOException {
        ftp.connect(FTPConstant.URl, FTPConstant.PORTA);
        ftp.login(FTPConstant.USUARIO, FTPConstant.SENHA);
        ftp.changeWorkingDirectory(FTPConstant.PATH);

        try (InputStream is = ftp.retrieveFileStream("sispec.xml")) {
            desc = DescriptorFactory.getDescriptorAtualizacaoInstance(is);
            ftp.completePendingCommand();
        }
    }

    public boolean hasAtualizacao() {

        return !AtualizadorSispec.MD5_REMOTO.equals(desc.getMd5());
    }

    public boolean isConectado() {
        return ftp.isConnected();
    }

    public String getMd5Ftp() {
        return desc.getMd5();
    }

    public void rodarMigracoes() throws IOException, SQLException {

        DAO dao;

        String[] sqlFiles = ftp.listNames(desc.getPathMigrations());

        for (String sqlFile : sqlFiles) {

            InputStream is = ftp.retrieveFileStream(sqlFile);

            dao = new DAO();

            dao.executaSqlScript(is, sqlFile.substring(sqlFile.indexOf("/") + 1, sqlFile.indexOf(".")));

            ftp.completePendingCommand();
            
            if (is != null) {
                is.close();
            }
        }
    }

    public void baixarExecutavel() throws IOException {

        try {
            System.out.println("Baixando executavel.");

            ftp.enterLocalPassiveMode();
            ftp.setFileType(FTP.BINARY_FILE_TYPE);

            String remoteFile = "sisbras.jar";
            File downloadFile = new File(AtualizadorSispec.DIR_JAR_REMOTO);

            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile));
            boolean success = ftp.retrieveFile(remoteFile, outputStream);
            outputStream.close();

            if (success) {
                System.out.println("executavel baixado com sucesso.");
            }

        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void fecharConexao() throws IOException {
        ftp.logout();
        ftp.disconnect();
    }

    public void listarDiretorio() throws IOException {

        String[] arq = ftp.listNames();

        System.out.println("Listando arquivos: \n");

        for (String f : arq) {
            System.out.println(f);
        }
    }

    public void exuceteAtualizacao(Thread t) {
        t1 = t;
        t1.start();
    }

    @Override
    public void run() {
        synchronized (this) {

            try {

                abrirConexao();

                if (hasAtualizacao()) {
                    rodarMigracoes();
                    baixarExecutavel();
                    AtualizadorSispec.atualizaMD5Remoto(getMd5Ftp());

                    DisplayAtualizacao
                            .getInstance()
                            .displayMessage("Nova versão do Sispec disponível",
                                    "Reinicie o sispec para baixar a atualização");
                    
                    Thread.sleep(5000);
                }

                fecharConexao();

            } catch (IOException | SQLException ex) {
                Logger.getLogger(ConexaoFTP.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(ConexaoFTP.class.getName()).log(Level.SEVERE, null, ex);
            }

            notify();
        }
    }
}
