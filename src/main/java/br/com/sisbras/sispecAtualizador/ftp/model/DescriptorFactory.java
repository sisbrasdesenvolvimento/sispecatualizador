package br.com.sisbras.sispecAtualizador.ftp.model;

import java.io.File;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author SAMUEL
 */
public class DescriptorFactory {

    private static final String PATH_LOCAL = "./sispec.xml";

    public static void create(DescriptorUpdate data) throws JAXBException {

        // create JAXB context and instantiate marshaller
        JAXBContext context = JAXBContext.newInstance(DescriptorUpdate.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Write to System.out
//        m.marshal(this, System.out);
        // Write to File
        m.marshal(data, new File(PATH_LOCAL));
    }

    public static DescriptorUpdate getDescriptorAtualizacaoInstance(InputStream is) {

        if (is == null) {
            return new DescriptorUpdate();
        }

        try {
            JAXBContext context = JAXBContext.newInstance(DescriptorUpdate.class);
            Unmarshaller um = context.createUnmarshaller();

            return (DescriptorUpdate) um.unmarshal(is);

        } catch (JAXBException ex) {
            return new DescriptorUpdate();
        }

    }
}
