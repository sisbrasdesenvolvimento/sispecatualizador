package br.com.sisbras.sispecAtualizador.controle;

import br.com.sisbras.sispecAtualizador.gui.FrmInicial;
import br.com.sisbras.sispecAtualizador.util.Util;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

/**
 *
 * @author pc2
 */
public class AtualizadorSispec {

    private FrmInicial tela;
    public static String PATH_LOCAL;
    public static String PATH_REMOTO;
    public static Properties PROPERTIES_LOCAL;
    public static Properties PROPERTIES_REMOTO;
    public static String MD5_LOCAL;
    public static String MD5_REMOTO;
    public static String DIR_JAR_REMOTO;
    public static String DIR_JAR_LOCAL;
    public static String DIR_CHAVE; // arquivo com chave de liberação
    public static final String NOME_JAR_PRODUCAO = "sisbras.jar";
    public static final String NOME_CHAVE = ";pass.key";
    public static final String NOME_PROPERTIES = "sispec.properties";

    public AtualizadorSispec(FrmInicial tela) {
        this.tela = tela;
    }

    public void renomeiaJarAntigo() {

        tela.setProcesso("Renomeando antigo executável: " + DIR_JAR_LOCAL.replaceAll(".jar", "_old.jar"));

        new File(DIR_JAR_LOCAL)
                .renameTo(new File(DIR_JAR_LOCAL.replaceAll(".jar", "_old.jar")));

        tela.setProcesso("Executável renomeado");
    }

    public void executarNovoJar() throws IOException {

        try {
            tela.setProcesso("Rodando executável de produção");
            java.awt.Desktop.getDesktop().open(new File(DIR_JAR_LOCAL));
        } catch (Exception e) {
            tela.setProcesso("Não foi possível rodar nova versão do executável - " + e.getMessage());
            throw e;
        }
    }

    public void transfereNovoJar() throws FileNotFoundException, IOException {

        try {

            tela.setProcesso("Transferindo executável remoto para pasta local...");

            FileInputStream fisRemoto = new FileInputStream(DIR_JAR_REMOTO);
            FileOutputStream fisLocal = new FileOutputStream(DIR_JAR_LOCAL);

            FileChannel fcRemoto = fisRemoto.getChannel();
            FileChannel fcLocal = fisLocal.getChannel();

            fcRemoto.transferTo(0, fcRemoto.size(), fcLocal);
            fisRemoto.close();
            fisLocal.close();

            tela.setProcesso("Transferência concluída com sucesso");
        } catch (Exception e) {
            tela.setProcesso("Não foi possível concluir a transferência do executável");
            throw e;
        }
    }

    public void inicializaPathLocal() {

        tela.setProcesso("processando diretório local do sistema");
        try {

            PATH_LOCAL = AtualizadorSispec.class.getResource("").getPath();

            PATH_LOCAL = PATH_LOCAL.replaceAll("%20", " ");

            if (PATH_LOCAL.indexOf(".jar") > 0) {

                PATH_LOCAL = PATH_LOCAL.substring(4).replaceFirst("/[^/]+\\.jar!.*$", "/");

                if (PATH_LOCAL.charAt(0) == ':') {

                    PATH_LOCAL = PATH_LOCAL.substring(1, PATH_LOCAL.length());
                } else if (PATH_LOCAL.charAt(0) == '/') {

                    PATH_LOCAL = PATH_LOCAL.substring(1, PATH_LOCAL.length());
                }
            } else {
                PATH_LOCAL = AtualizadorSispec.class.getResource("/").getPath();
            }

            tela.setProcesso("Diretório local: " + PATH_LOCAL);

        } catch (Exception e) {
            tela.setProcesso("Erro ao processar diretório local: " + e.getMessage());
            throw e;
        }
    }

    public boolean inicializaPathRemoto() throws Exception {
        try {

            String dir = PROPERTIES_LOCAL.getProperty("pathRemoto");

            if (new File(dir.substring(0, dir.length() - 1)).isDirectory()) {

                PATH_REMOTO = dir;
                tela.setProcesso("Diretório remoto processado com sucesso: " + PATH_REMOTO);
                return true;
            } else {
                tela.setProcesso("Diretório remoto não encontrado");
                throw new Exception();
            }

        } catch (Exception e) {
            tela.setProcesso("Não foi possível processar diretório remoto: " + e.getMessage());
            throw e;
        }
    }

    public void inicializaPropertiesLocal() throws FileNotFoundException, IOException {

        try {

            File file = new File(PATH_LOCAL + NOME_PROPERTIES);

            PROPERTIES_LOCAL = new Properties();
            try (FileInputStream fis = new FileInputStream(file)) {
                PROPERTIES_LOCAL.load(fis);
            }

            tela.setProcesso("Arquivo de propriedades local carregado com sucesso");

        } catch (FileNotFoundException e) {
            tela.setProcesso("Erro ao carregar arquivo de propriedades local " + e.getMessage());
            throw e;
        } catch (IOException e) {
            tela.setProcesso("Erro ao carregar arquivo de propriedades local " + e.getMessage());
            throw e;
        }
    }

    public void inicializaPropertiesRemoto() throws FileNotFoundException, IOException {

        try {

            File file = new File(PATH_REMOTO + NOME_PROPERTIES);

            PROPERTIES_REMOTO = new Properties();
            try (FileInputStream fis = new FileInputStream(file)) {
                PROPERTIES_REMOTO.load(fis);
            }

            tela.setProcesso("Arquivo de propriedades remoto carregado com sucesso");

        } catch (FileNotFoundException e) {
            tela.setProcesso("Erro ao carregar arquivo de propriedades remoto " + e.getMessage());
            throw e;
        } catch (IOException e) {
            tela.setProcesso("Erro ao carregar arquivo de propriedades remoto " + e.getMessage());
            throw e;
        }
    }

    public boolean inicializaDirJarRemoto() {
        DIR_JAR_REMOTO = PATH_REMOTO + NOME_JAR_PRODUCAO;

        tela.setProcesso("Procurando executável remoto: " + DIR_JAR_REMOTO);

        File file = new File(DIR_JAR_LOCAL);

        if (file.isFile()) {
            tela.setProcesso("Executável encontrado");
            return true;
        } else {
            tela.setProcesso("Executável não encontrado");
            return false;
        }
    }

    public boolean inicializaDirJarLocal() {
        DIR_JAR_LOCAL = PATH_LOCAL + NOME_JAR_PRODUCAO;
        tela.setProcesso("Procurando executável local: " + DIR_JAR_LOCAL);

        File file = new File(DIR_JAR_LOCAL);

        if (file.isFile()) {
            tela.setProcesso("Executável encontrado");
            return true;
        } else {
            tela.setProcesso("Executável não encontrado");
            return false;
        }
    }

    public void inicializaMD5Local() throws NoSuchAlgorithmException, FileNotFoundException, IOException {

        try {

            MD5_LOCAL = PROPERTIES_LOCAL.getProperty("MD5");

            if (MD5_LOCAL != null
                    && !MD5_LOCAL.isEmpty()) {
                // preenchido
            } else {

                tela.setProcesso("Código de versão local não encontrado no arquivo de propriedades");

                tela.setProcesso("Calculando código de versão...");

                MD5_LOCAL = Util.calculaMD5(DIR_JAR_LOCAL);


                PROPERTIES_LOCAL.setProperty("MD5", MD5_LOCAL);
                PROPERTIES_LOCAL.store(new FileOutputStream(PATH_LOCAL + NOME_PROPERTIES), null);
            }
            tela.setProcesso("Código de versão do executável local: " + MD5_LOCAL);

        } catch (Exception e) {
            tela.setProcesso("Erro ao processar código de versão do executavel local " + e.getMessage());
            throw e;
        }
    }

    public void inicializaMD5Remoto() throws NoSuchAlgorithmException, FileNotFoundException, IOException {

        try {

            MD5_REMOTO = PROPERTIES_REMOTO.getProperty("MD5");

            if (MD5_REMOTO != null
                    && !MD5_REMOTO.isEmpty()) {
                // preenchido
            } else {

                tela.setProcesso("Código de versão remoto não encontrado no arquivo de propriedades");

                tela.setProcesso("Calculando código de versão...");

                MD5_REMOTO = Util.calculaMD5(DIR_JAR_REMOTO);

                PROPERTIES_REMOTO.setProperty("MD5", MD5_REMOTO);
                PROPERTIES_REMOTO.store(new FileOutputStream(PATH_REMOTO + NOME_PROPERTIES), null);
            }

            tela.setProcesso("Código de versão do executável remoto: " + MD5_REMOTO);

        } catch (Exception e) {
            tela.setProcesso("Erro ao processar código de versão do executavel remoto " + e.getMessage());
            throw e;
        }
    }

    public boolean isVersaoDiferente() {

        try {

            boolean resposta = !MD5_LOCAL.equalsIgnoreCase(MD5_REMOTO);

            if (resposta) {
                tela.setProcesso("Versão local diferente da versão remota");
                tela.getLbInfo().setText("Atualizando versão");
                return true;
            } else {
                tela.setProcesso("Versão local igual a versão remota");
                return false;
            }

        } catch (Exception e) {
            tela.setProcesso("Não foi possível comparar as versões de sistema. "
                    + "Os codigos de versão de sistema não foram processados corretamente");
            throw e;
        }
    }

    public void inicializaChave() {

        try {

            DIR_CHAVE = PATH_REMOTO + NOME_CHAVE;

            if (new File(DIR_CHAVE).isDirectory()) {

                tela.setProcesso("Chave " + NOME_CHAVE + " processada");
            } else {

                tela.setProcesso("Chave " + NOME_CHAVE + " não encontrada no diretório remoto");
            }

        } catch (Exception e) {
            tela.setProcesso("Não foi possível processar a chave " + NOME_CHAVE + " - " + e.getMessage());
            throw e;
        }
    }

    public void transfereChave() throws FileNotFoundException, IOException {

        try {

            FileInputStream fisRemoto = new FileInputStream(DIR_CHAVE);
            FileOutputStream fisLocal = new FileOutputStream(PATH_LOCAL + NOME_CHAVE);

            FileChannel fcRemoto = fisRemoto.getChannel();
            FileChannel fcLocal = fisLocal.getChannel();

            fcRemoto.transferTo(0, fcRemoto.size(), fcLocal);
            fisRemoto.close();
            fisLocal.close();

            tela.setProcesso("Chave " + NOME_CHAVE + " transferida para diretório local");

        } catch (Exception e) {
            tela.setProcesso("Não foi possível transferir a chave " + NOME_CHAVE + " para diretório local");
            throw e;
        }

    }

    public void atualizaMD5Local() throws IOException {

        PROPERTIES_LOCAL.setProperty("MD5", MD5_REMOTO);
        PROPERTIES_LOCAL.store(new FileOutputStream(PATH_LOCAL + NOME_PROPERTIES), null);

        tela.setProcesso("Código de versão do sistema atualizado");
    }
    
    public static void atualizaMD5Remoto(String md5) throws IOException {

        PROPERTIES_LOCAL.setProperty("MD5", md5);
        PROPERTIES_LOCAL.store(new FileOutputStream(PATH_REMOTO + NOME_PROPERTIES), null);
    }

    public void respostaBoleto() throws Exception {

        try {

            String resposta = PROPERTIES_REMOTO.getProperty("IMPRIME_BOLETO");

            PROPERTIES_LOCAL.setProperty("IMPRIME_BOLETO", resposta);
            PROPERTIES_LOCAL.store(new FileOutputStream(PATH_LOCAL + NOME_PROPERTIES), null);

            tela.setProcesso("Configuração do boleto processada com sucesso");

        } catch (Exception ex) {

            tela.setProcesso("Erro ao processar configuração do boleto");

            throw ex;

        }
    }
}
