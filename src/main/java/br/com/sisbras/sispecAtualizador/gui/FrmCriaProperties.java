package br.com.sisbras.sispecAtualizador.gui;

import br.com.sisbras.sispecAtualizador.controle.AtualizadorSispec;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

/**
 *
 * @author pc2
 */
public class FrmCriaProperties extends javax.swing.JDialog {

    private boolean ok;
    private Border bordaPadrao;
    private LineBorder bordaVermelha = new LineBorder(Color.RED);

    public FrmCriaProperties(java.awt.Dialog parent, boolean modal) {
        super(parent, modal);
        initComponents();
        initFrame();
        this.setLocationRelativeTo(parent);
    }

    private void initFrame() {

        class EventoTecla extends KeyAdapter {

            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);

                Component c = ((Component) e.getSource());
                switch (e.getKeyCode()) {

                    case KeyEvent.VK_ENTER:
                        c.transferFocus();
                        break;
                    case KeyEvent.VK_ESCAPE:
                        c.transferFocusBackward();
                        break;
                }
            }
        }

        class FocoTecla extends FocusAdapter {

            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                verificaPreenchido((Component) e.getSource());
            }
        }

        bordaPadrao = txtIpServidor.getBorder();

        txtIpServidor.addKeyListener(new EventoTecla());
        txtPorta.addKeyListener(new EventoTecla());
        txtBanco.addKeyListener(new EventoTecla());
        txtUsuario.addKeyListener(new EventoTecla());
        txtSenha.addKeyListener(new EventoTecla());
        txtDirServidor.addKeyListener(new EventoTecla());

        txtIpServidor.addFocusListener(new FocoTecla());
        txtPorta.addFocusListener(new FocoTecla());
        txtBanco.addFocusListener(new FocoTecla());
        txtUsuario.addFocusListener(new FocoTecla());
        txtSenha.addFocusListener(new FocoTecla());
        txtDirServidor.addFocusListener(new FocoTecla());

        txtIpServidor.requestFocusInWindow();

    }

    public boolean isOk() {
        return ok;
    }

    private boolean camposObrigatorios() {
        return !txtIpServidor.getText().isEmpty()
                && !txtPorta.getText().isEmpty()
                && !txtBanco.getText().isEmpty()
                && !txtUsuario.getText().isEmpty()
                && !String.valueOf(txtSenha.getPassword()).isEmpty()
                && !txtDirServidor.getText().isEmpty();
    }

    private void verificaPreenchido(Component c) {

        if (c instanceof JTextField) {

            JTextField txt = (JTextField) c;
            txt.setText(txt.getText().toUpperCase());

            if (txt.getText().isEmpty()) {
                txt.setBorder(bordaVermelha);
            } else {
                txt.setBorder(bordaPadrao);
            }

        } else if (c instanceof JPasswordField) {

            JPasswordField txt = (JPasswordField) c;

            if (String.valueOf(txt.getPassword()).isEmpty()) {
                txt.setBorder(bordaVermelha);
            } else {
                txt.setBorder(bordaPadrao);
            }
        }
        c.repaint();
    }

    private void criaProperties() throws IOException, Exception {

        try {

            String caminhoArquivo = AtualizadorSispec.PATH_LOCAL + AtualizadorSispec.NOME_PROPERTIES;

            try {

                // cria arquivo .properties vazio no diretorio
                try (BufferedWriter bf = new BufferedWriter(new FileWriter(caminhoArquivo))) {
                }
            } catch (IOException e) {
                try {

                    // cria arquivo .properties vazio no diretorio
                    try (BufferedWriter bf = new BufferedWriter(new FileWriter(caminhoArquivo))) {
                    }

                } catch (Exception ex) {
                    throw e;
                }
            }

            // carrega arquivo na memoria
            File file = new File(caminhoArquivo);
            AtualizadorSispec.PROPERTIES_LOCAL = new Properties();
            try (FileInputStream fis = new FileInputStream(file)) {
                AtualizadorSispec.PROPERTIES_LOCAL.load(fis);
            }

            // escreve as propriedades no arquivo
            AtualizadorSispec.PROPERTIES_LOCAL.setProperty("IP", txtIpServidor.getText());
            AtualizadorSispec.PROPERTIES_LOCAL.setProperty("PORTA", txtPorta.getText());
            AtualizadorSispec.PROPERTIES_LOCAL.setProperty("DATABASE", txtBanco.getText());
            AtualizadorSispec.PROPERTIES_LOCAL.setProperty("USER", txtUsuario.getText());

            br.com.sisbrastools.encrypt.Encriptador encriptador 
                    = new br.com.sisbrastools.encrypt.Encriptador(String.valueOf(txtSenha.getPassword()));

            AtualizadorSispec.PROPERTIES_LOCAL.setProperty("SENHA", encriptador.doEncrypt(AtualizadorSispec.PROPERTIES_LOCAL));
            AtualizadorSispec.PROPERTIES_LOCAL.setProperty("IMPRIME_BOLETO", "");
            AtualizadorSispec.PROPERTIES_LOCAL.setProperty("MD5", "");

            if (!txtDirServidor.getText().endsWith("\\")) {
                txtDirServidor.setText(txtDirServidor.getText() + "\\");
            }

            AtualizadorSispec.PROPERTIES_LOCAL.setProperty("pathRemoto", txtDirServidor.getText());

            AtualizadorSispec.PROPERTIES_LOCAL.store(new FileOutputStream(caminhoArquivo), null);

        } catch (IOException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btSalvar = new javax.swing.JButton();
        btCancelar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        txtIpServidor = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtDirServidor = new javax.swing.JTextField();
        txtPorta = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        txtBanco = new javax.swing.JTextField();
        txtSenha = new javax.swing.JPasswordField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Sispec - Criar Arquivo de Propriedades");

        btSalvar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btSalvar.setText("Salvar");
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });
        btSalvar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btSalvarKeyPressed(evt);
            }
        });

        btCancelar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btCancelar.setText("Cancelar");
        btCancelar.setFocusable(false);
        btCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCancelarActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtIpServidor.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        txtIpServidor.setForeground(new java.awt.Color(0, 0, 153));
        txtIpServidor.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtIpServidorFocusLost(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabel2.setText("Porta Banco");

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabel3.setText("Nome Base de Dados");

        txtDirServidor.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        txtDirServidor.setForeground(new java.awt.Color(0, 0, 153));

        txtPorta.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        txtPorta.setForeground(new java.awt.Color(0, 0, 153));

        jLabel5.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabel5.setText("Senha Banco");

        jLabel4.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabel4.setText("Usuário Banco");

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabel1.setText("IP Banco Servidor");

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabel6.setText("Diretório Sistema Servidor");

        txtUsuario.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        txtUsuario.setForeground(new java.awt.Color(0, 0, 153));
        txtUsuario.setText("sa");

        txtBanco.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        txtBanco.setForeground(new java.awt.Color(0, 0, 153));

        txtSenha.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        txtSenha.setForeground(new java.awt.Color(0, 0, 153));
        txtSenha.setText("123456");
        txtSenha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSenhaActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jLabel7.setText("Padrão linux: \\\\servidor\\u\\sispec\\");

            jLabel8.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
            jLabel8.setText("Padrão windows: z:\\sispec\\");

                jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/kghostview_21x22.png"))); // NOI18N
                jButton1.setFocusable(false);
                jButton1.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButton1ActionPerformed(evt);
                    }
                });

                javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
                jPanel1.setLayout(jPanel1Layout);
                jPanel1Layout.setHorizontalGroup(
                    jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel8))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(62, 62, 62))
                            .addComponent(txtPorta)
                            .addComponent(txtIpServidor)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtDirServidor)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtUsuario)
                            .addComponent(txtBanco)
                            .addComponent(txtSenha))
                        .addGap(22, 22, 22))
                );
                jPanel1Layout.setVerticalGroup(
                    jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIpServidor, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPorta, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBanco, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDirServidor, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel8)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btCancelar))
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );
                layout.setVerticalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 462, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                );

                pack();
            }// </editor-fold>//GEN-END:initComponents

    private void btCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCancelarActionPerformed
        ok = false;
        this.dispose();
    }//GEN-LAST:event_btCancelarActionPerformed

    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
        try {
            if (!camposObrigatorios()) {
                return;
            }
            criaProperties();
            ok = true;
            this.dispose();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(rootPane, "Erro ao criar arquivo de propriedades\n" + ex.getMessage(), "Tratamento de execução", JOptionPane.ERROR_MESSAGE);
        } 
    }//GEN-LAST:event_btSalvarActionPerformed

    private void txtIpServidorFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtIpServidorFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIpServidorFocusLost

    private void txtSenhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSenhaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSenhaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int returnVal = chooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            File file = chooser.getSelectedFile();

            txtDirServidor.setText(file.getAbsolutePath() + "\\");

        } else {
            // nothing happens
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void btSalvarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btSalvarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            btSalvar.transferFocusBackward();
        } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btSalvar.doClick();
        }
    }//GEN-LAST:event_btSalvarKeyPressed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCancelar;
    private javax.swing.JButton btSalvar;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtBanco;
    private javax.swing.JTextField txtDirServidor;
    private javax.swing.JTextField txtIpServidor;
    private javax.swing.JTextField txtPorta;
    private javax.swing.JPasswordField txtSenha;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
