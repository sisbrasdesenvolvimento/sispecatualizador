package br.com.sisbras.sispecAtualizador.gui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author pc2
 */
public class PainelSplash extends JPanel {

    private BufferedImage splash = null;

    public PainelSplash() {
        try {
            this.splash = ImageIO.read(getClass().getResource("/img/splash.png"));
        } catch (IOException ex) {
        } 
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (splash != null) {
            g.drawImage(splash, 0, 0, this);
        }
    }
}
