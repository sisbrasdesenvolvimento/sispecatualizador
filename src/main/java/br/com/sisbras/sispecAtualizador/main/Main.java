package br.com.sisbras.sispecAtualizador.main;

import br.com.sisbras.sispecAtualizador.controle.AtualizadorSispec;
import br.com.sisbras.sispecAtualizador.ftp.ConexaoFTP;
import br.com.sisbras.sispecAtualizador.gui.FrmCriaProperties;
import br.com.sisbras.sispecAtualizador.gui.FrmInicial;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author pc2
 */
public class Main {

    public static void main(String[] args) throws IOException {

        new CreateXml().create(args);

        final FrmInicial tela = new FrmInicial(null, true);

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    tela.getBarraProgresso().setIndeterminate(true);

                    AtualizadorSispec atualizador = new AtualizadorSispec(tela);

                    Thread.sleep(2000);

                    atualizador.inicializaPathLocal();

                    try {

                        atualizador.inicializaPropertiesLocal();

                    } catch (FileNotFoundException e) {

                        tela.setProcesso("Tentando criar arquivo de propriedades");
                        FrmCriaProperties frm = new FrmCriaProperties(tela, true);
                        frm.setVisible(true);

                        if (frm.isOk()) {
                            tela.setProcesso("Arquivo de propriedades criado, tentando executar proximos passos");
                        } else {
                            tela.setProcesso("Falha ao criar arquivo de propriedades");
                            throw new Exception("Falha ao criar arquivo de propriedades");
                        }
                    }

                    atualizador.inicializaPathRemoto();

                    atualizador.inicializaPropertiesRemoto();

                    atualizador.respostaBoleto();

                    atualizador.inicializaDirJarLocal();

                    atualizador.inicializaMD5Local();

                    atualizador.inicializaDirJarRemoto();

                    atualizador.inicializaMD5Remoto();

                    ConexaoFTP ftp = new ConexaoFTP();
                    Thread t1 = new Thread(ftp);

                    ftp.exuceteAtualizacao(t1);

                    if (atualizador.isVersaoDiferente()) {

                        atualizador.renomeiaJarAntigo();

                        atualizador.inicializaChave();

                        atualizador.transfereChave();

                        atualizador.transfereNovoJar();

                        atualizador.atualizaMD5Local();

                    } else {
                        // não precisa atualizar
                    }

                    atualizador.executarNovoJar();

                    tela.dispose();

                    tela.escreveProcessos();

                    synchronized (t1) {
                        try {
                            if (t1.isAlive()) {
                                System.out.println("Aguardando o t1 completar");
                                t1.wait();
                            }
                            System.out.println("completou t1");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    System.exit(0);

                } catch (Exception ex) {

                    JOptionPane.showMessageDialog(tela, "Erro ao inicializar as proriedades do sistema"
                            + "\nEntre em contato com a Sisbras", "Tratamento de execuÃ§Ã£o", JOptionPane.ERROR_MESSAGE);

                    ex.printStackTrace();
                    tela.escreveProcessos();
                    System.exit(1);
                }
            }
        });
        t.start();

        tela.setVisible(true);
    }
}
